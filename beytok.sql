-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: db2.ho.ua
-- Час створення: Вер 30 2018 р., 23:03
-- Версія сервера: 5.6.41-log
-- Версія PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `beytok`
--

-- --------------------------------------------------------

--
-- Структура таблиці `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `time`, `file`) VALUES
(20, 'Choose a Topic That Interests YOU', 'There&rsquo;s an old maxim that states, &ldquo;No fun for the writer, no fun for the reader.&rdquo; No matter what industry you&rsquo;re working in, as a blogger, you should live and die by this statement.\r\n\r\nBefore you do any of the following steps, be sure to pick a topic that actually interests you. Nothing &ndash; and I mean NOTHING &ndash; will kill a blog post more effectively than a lack of enthusiasm from the writer. You can tell when a writer is bored by their subject, and it&rsquo;s so cringe-worthy it&rsquo;s a little embarrassing.\r\n\r\nHow to write a blog post about boring topics\r\nDon\'t go there.\r\n\r\nI can hear your objections already. &ldquo;But Dan, I have to blog for a cardboard box manufacturing company.&rdquo; I feel your pain, I really do. During the course of my career, I&rsquo;ve written content for dozens of clients in some less-than-thrilling industries (such as financial regulatory compliance and corporate housing), but the hallmark of a professional blogger is the ability to write well about any topic, no matter how dry it may be. Blogging is a lot easier, however, if you can muster at least a little enthusiasm for the topic at hand.\r\n\r\nYou also need to be able to accept that not every post is going to get your motor running. Some posts will feel like a chore, but if you have editorial control over what you write about, then choose topics you&rsquo;d want to read &ndash; even if they relate to niche industries. The more excited you can be about your topic, the more excited your readers will be when they&rsquo;re reading it.\r\n\r\nIf you\'re really desperate for inspiration, check out our list of eight blog topic generators to get you going.', '2018-09-30 09:37:56', 1),
(21, 'Write an Outline For Your Post', 'Great blog posts don&rsquo;t just happen. Even the best bloggers need a rough idea to keep them on-track. This is where outlines come in.\r\n\r\nAn outline doesn&rsquo;t need to be lengthy, or even detailed &ndash; it&rsquo;s just a rough guide to make sure you don&rsquo;t ramble on and on about something tangential to your topic.\r\n\r\nFor example, this is the outline for this post that I sent to my editor before getting to work:\r\n\r\nIntroduction\r\n\r\n[Quick summary explaining what the blog post will cover]\r\n\r\nSection 1 &ndash; Planning a Blog Post\r\n\r\n- Things bloggers should do before putting pen to paper &ndash; outlining, research etc.\r\n\r\nSection 2 &ndash; Writing a Blog Post\r\n\r\n- Tips on how to focus on writing, productivity tips for bloggers\r\n\r\nSection 3 &ndash; Rewriting/Editing a Blog Post\r\n\r\n- Self-editing techniques, things to watch out for, common blogging mistakes\r\n\r\nSection 4 &ndash; Optimizing a Blog Post\r\n\r\n- How to optimize a blog post for on-page SEO, social shares/engagement, etc.\r\n\r\nSection 5 &ndash; Conclusion\r\n\r\n- Wrap-up\r\n\r\nThe purpose of this outline is to make sure I know what I plan to cover, in what order the various sections will appear, and some bare-bones details of what each section will include.\r\n\r\nOutlines keep you honest. They stop you from indulging in poorly thought-out metaphors about driving and keep you focused on the overall structure of your post. Sometimes I&rsquo;ll write a more thorough outline (and sometimes I won&rsquo;t bother with one at all), but most of the time, something like the outline above is perfectly acceptable.\r\n\r\nWhether you write your outline in your word processor, on a piece of paper, or even scribbled on a bar napkin, do whatever works for you to keep you focused.', '2018-09-30 09:40:35', 2),
(22, 'Do Your Research', 'One of the biggest secrets professional bloggers (myself included) don&rsquo;t want you to know is that we don&rsquo;t actually know everything. Truth be told, sometimes we don&rsquo;t know anything about a topic before we sit down to write about it.\r\n\r\nPro tip: you don\'t actually need a passport to write a travel marketing post.\r\n\r\nThis doesn&rsquo;t mean that all bloggers are insincere fakers. On the contrary, many bloggers&rsquo; natural curiosity is what makes them great at what they do. If you blog for a living, you have to be comfortable jumping from one topic to the next, even if you don&rsquo;t know anything about it. What allows us to do this, and to write authoritatively about subject areas that are new to us, is knowing how to properly research a blog post.\r\n\r\nIt almost goes without saying, but relying solely on Wikipedia as a primary source is almost always a bad idea. Yes, Wikipedia does have thousands of excellently researched articles, but it&rsquo;s not infallible, and erroneous facts do make their way into articles without site editors noticing. Plus, every verifiable fact on the site is cited from links elsewhere on the web, so why cite the middleman?\r\n\r\nLou Diamond Phillips was a total beast in &lsquo;La Bamba.&rsquo;\r\n\r\nIf you&rsquo;re relying on third-party information to write your blog post, choose authoritative sources. Official associations, government websites, heavily cited research papers, and preeminent industry experts are all good examples. Nobody is right all the time, though, so approach every source with a the practiced skepticism of a journalist and question everything until you&rsquo;re positive your information is solid.', '2018-09-30 09:41:29', 3),
(23, 'Check Your Facts', 'A few years ago, I edited a piece written by a colleague focusing on the highlights of a major technology conference. The writer, under a seriously tight deadline, had done a bang-up job of writing great copy in virtually no time, but he failed to properly check his facts. He cited an article from Forbes in which the writer claimed Steve Jobs was using PowerPoint on stage &ndash; something that never happened. It was lazy journalism on the part of the Forbes writer, and an easy mistake to make on my colleague&rsquo;s part, but the result was the same; one poorly researched article directly impacted another because both writers failed to do their due diligence.\r\n\r\nAll it takes to tank your credibility is one glaring error. Everyone makes mistakes, but it&rsquo;s crucial to avoid gaffes like this. If you&rsquo;re just starting out, your credibility and authority will take a major hit if you publish inaccurate information, and even if you have a blog with millions of loyal readers, your regulars will be all too eager to jump all over your mistake &ndash; just take a look in the comment sections of publications such as Wired or TechCrunch to see how quickly this can happen.\r\n\r\nIn the event that you fall prey to a well-executed hoax, repeat widely circulated misinformation, or simply make a mistake, own up to it right away and be transparent about your edits. If you try to slip something past your readers, you can bet that they&rsquo;ll call you out on it, further compounding the damage. Be honest, be accountable, and fix it &ndash; fast.', '2018-09-30 09:42:04', 4),
(24, 'Writing a Great Headline', 'Everyone and their grandmother has an opinion about headlines. Some say you should be as specific as possible (to avoid misleading your readers and manage their expectations), while others recommend taking a more abstract approach. Vague headlines might work just fine if you&rsquo;re Seth Godin, but for most of us, being specific is better.\r\n\r\nSome headlines practically write themselves.\r\n\r\nThere are two main approaches you can take to writing blog post headlines. You can either decide on your final headline before you write the rest of your post (and use your headline to structure your outline), or you can write your blog post with a working title and see what fits when you&rsquo;re done.\r\n\r\nPersonally, I don&rsquo;t adhere to a rigid strategy one way or the other. Sometimes I&rsquo;ll come up with a strong headline from the outset and stick with it, whereas other posts will take a lot more work. Although sites such as Upworthy arguably ruined internet writing with their clickbait headlines, the process behind the site&rsquo;s headlines has merit, as it forces you to really think about your post and how to grab your audience&rsquo;s attention.\r\n\r\nYour approach to headlines should also vary depending on your audience. For example, let&rsquo;s look at these super-specific headlines from around the web:\r\n\r\nHow Our Side Project Generated $51,365 in 60 Days\r\nHow Lua&rsquo;s CEO Built an Enterprise Messaging App That Boosts Open Rates From 20% to 98%\r\n5 Things We Did in 2014 to Grow by 1059%\r\nThe exact figures presented in these headlines are all framed within a context of providing actionable advice to other marketers and startups. &ldquo;Case study&rdquo; blog posts like this often perform well, due to their transparent nature (which pulls the curtain back from successful growing businesses and the people who run them) and the &ldquo;how-to&rdquo; angle (which attracts people who want to accomplish the same thing by following real-world examples).\r\n\r\nPeople LOVE how-to articles.\r\n\r\nThat&rsquo;s all well and good if that&rsquo;s what you&rsquo;re looking for &ndash; which, in my case, is rare. I didn&rsquo;t read any of these posts, simply because it seems that at least half of the blog posts in my RSS feed are structured in this fashion (including this one). They&rsquo;re great for the sake of example, but I glossed right over them because they&rsquo;re so similar to the dozens of other posts I see every day telling me three hacks to grow my startup by X percent in Y months.\r\n\r\nAnother common technique is posing a question in your headline. Done well, this can be extraordinarily effective, as it is in these examples:\r\n\r\nCan an Algorithm Write a Better News Story Than a Human Reporter?\r\nWould You Be Part of a Crowdsourced Environmental Warning System?\r\nWhat Do Uber, Zenefits, and Public Health in a Kenyan Slum Have in Common?\r\nHowever, this technique is also growing tiresome, and fewer publications are utilizing it these days (thankfully alongside the always-irksome &ldquo;You won&rsquo;t believe&hellip;&rdquo; headline). If you opt for asking questions in your headlines, be sure it&rsquo;s a question your audience will be genuinely interested in.  \r\n\r\nWriting headlines for blog posts is as much an art as it is a science, and probably warrants its own post, but for now, all I&rsquo;d advise is experimenting with what works for your audience. If your readers want hyper-specific case studies on how to do stuff, by all means let &lsquo;em have it. Don&rsquo;t, however, do something just because someone else is, especially if it&rsquo;s not resonating with your audience.', '2018-09-30 09:51:27', 5),
(25, 'The Writing Part', 'So, you&rsquo;ve done your research, settled on a headline (or at least a working title), and now you&rsquo;re ready to actually write a blog post. So get to it.\r\n\r\nBe sure to actually turn your computer on before you start writing.\r\n\r\nSimilarly to headlines, there are two main approaches to writing a blog post. You can either sit down and write an entire draft in a single sitting (my preferred workflow), or you can chip away at it gradually over time. There is no right or wrong answer here &ndash; only whatever works for you.\r\n\r\nHowever, I&rsquo;d recommend getting as much done in one session as possible. This makes it easier to stay focused on the topic, minimizes the chance that you&rsquo;ll forget crucial points, and also lets you get the damned thing out of your hair faster.\r\n\r\nEven if you work more effectively in short bursts, try to maximize the amount of writing you get done in those sessions. The more times you have to revisit a draft, the more tempting it is to add a little here, and a little there, and before you know it, you&rsquo;ve gone wildly off-topic. Get as much done as you can in a single sitting even if you prefer to draft a blog post over three or four writing sessions.\r\n\r\nLike most skills, writing becomes easier and more natural the more you do it. When you first start, you might find that it takes a week (or longer) to write a post, but with practice, you&rsquo;ll be knocking out great posts in hours. Unfortunately, there are no &ldquo;hacks&rdquo; or shortcuts when it comes to writing &ndash; you have to put in the time at the coalface.\r\n\r\nNOTE: A lot of people struggle with writing introductions. A great strategy is to write the introduction last. Just get into the meat of the blog post, and worry about the introduction later. Here are five easy ways to write a great introduction.', '2018-09-30 09:52:53', 6),
(26, 'Using Images Effectively', 'Writing for the web is an entirely different animal than writing for print. Oftentimes, people simply don&rsquo;t have the time, will, or ability to focus on lengthy blog posts without some visual stimulation. Even a well-formatted blog post consisting solely of text is likely to send your reader screaming back to Reddit or Twitter within minutes, which is why it&rsquo;s so important to include images in your posts.\r\n\r\nImages Help Your Blog Post Flow More Effectively\r\nOne of the most important reasons to include images in your blog posts is to break up the text. Many people scan blog posts rather than pore over every word, and interspersing images throughout the copy will make your post seem less intimidating and more visually appealing.\r\n\r\nImages Make Great Visual Punchlines\r\nEveryone likes a good laugh, and a well-chosen image can help lighten the tone of your posts and inject some much-needed humor into a piece. This can be particularly effective if you&rsquo;re writing about a dry (or flat-out boring) topic.\r\n\r\nThis image has nothing to do with blogging.', '2018-09-30 09:53:44', 7),
(27, 'The Editing Part', 'Actually writing a blog post is hard. Editing a blog post is harder. Many people mistakenly assume that editing is simply striking through sentences that don&rsquo;t work or fixing grammatical errors. Although sentence structure and grammar are both very important, editing is about seeing the piece as a whole and, sometimes, being willing to sacrifice words (and the hours it took to write them) for the sake of cohesion.\r\n\r\nI won&rsquo;t explicitly tell you to check your spelling and grammar &ndash; you should be doing that anyway. I will, however, offer some self-editing tips and suggestions on how to tighten up your writing so that it packs a punch and keeps your readers scrolling.\r\n\r\nAvoid Repetition\r\nFew things are more jarring to read than repetition of certain words or phrases. Once you&rsquo;re done with the first draft of your blog post, read through it and check for words that can be replaced to avoid repeating yourself.\r\n\r\nRepetition - avoid it.\r\n\r\nBONUS: Every writer has a &ldquo;crutch&rdquo; word or phrase. This is a word that, no matter how carefully they might try, the writer simply cannot help themselves from including in their work. Identify what your crutch word is, be vigilant, and make sure it doesn&rsquo;t appear more often than it needs to.', '2018-09-30 10:26:05', 8),
(28, 'Don&rsquo;t Be Afraid to Make Cuts or Adapt on the Fly', 'You may have forgotten, but I originally included a section in the example outline for this post that dealt with optimizing blog posts for SEO. I fully intended to write this section, but when I looked at how my first draft was shaping up, I realized this was too substantial a topic to tackle in an already lengthy post. As a result, I made the decision to cut this section from the post altogether. I purposefully left the outline intact to demonstrate that you shouldn&rsquo;t be afraid to make editorial decisions like this.\r\n\r\nUnless there&rsquo;s something you absolutely MUST include (say, a section that your sales or managerial team is expecting in a post that you agreed to deliver), your outline is not carved in stone. Remember &ndash; an outline is a guide, not an immutable series of commandments. If something doesn&rsquo;t work, whether it be a sentence, a paragraph, or even a whole section, don&rsquo;t hesitate to make the cut. Be ruthless with your work.', '2018-09-30 10:27:01', 9),
(29, 'Keep Sentences Short and Paragraphs Shorter', 'Nothing will intimidate or outright anger a reader faster than huge walls of text. It&rsquo;s a common mistake for inexperienced bloggers to make, and one I see far too often in a lot of online articles.\r\n\r\nSentences should be as short as possible. They&rsquo;re easier to read, making your audience&rsquo;s job easier. Shorter sentences also reduce the likelihood of going off on tangents. For example, I recently came across a sentence in an opinion piece in Wired that had no fewer than seven subordinate clauses, an editorial sin of almost unimaginable magnitude.\r\n\r\nParagraphs should also be short and sweet. The shorter the paragraph, the more likely your readers are to keep going. The &ldquo;rules&rdquo; of paragraph structure have been bent a little since web-based publishing became the norm, but try to keep individual ideas isolated to their own neat, short little paragraph.\r\n\r\nAccept That Your Blog Post Will Never Be Perfect\r\nThere&rsquo;s no such thing as a perfect post, and the sooner you come to terms with this, the better.\r\n\r\nI&rsquo;m not advocating for publishing sloppy work, nor am I saying you shouldn&rsquo;t be obsessive about the details. I am saying, however, that even the best blog posts could always be better, but time is always against us. Again, unless you&rsquo;re Seth Godin, you probably need to publish more than one post a month, so agonizing over every post will sap you of the desire to write and waste precious time &ndash; not to mention likely to incur the wrath of your editor or content manager.\r\n\r\nMake every post as good as it can be, learn from the experience, then move on.', '2018-09-30 10:28:05', 10);

-- --------------------------------------------------------

--
-- Структура таблиці `pic_file`
--

CREATE TABLE `pic_file` (
  `id` int(11) NOT NULL,
  `path` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `pic_file`
--

INSERT INTO `pic_file` (`id`, `path`) VALUES
(1, '/upload/1538300276_about-img-2.png'),
(2, '/upload/1538300435_about-img-4.png'),
(3, '/upload/1538300489_about-img-3.png'),
(4, '/upload/1538300524_about-img-5.png'),
(5, '/upload/1538301087_about-img.jpg'),
(6, '/upload/1538301173_bd.jpg'),
(7, '/upload/1538301224_about-img-1.png'),
(8, '/upload/1538303165_about-img-2.png'),
(9, '/upload/1538303221_about-img-3.png'),
(10, '/upload/1538303285_about-img-5.png');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `pic_file`
--
ALTER TABLE `pic_file`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблиці `pic_file`
--
ALTER TABLE `pic_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
