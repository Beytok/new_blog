<?php

class Validator
{
  //array of errors
  static public $errors = array(
    'title' => 'Title cannot be empty.',
    'content' => 'Post cannot be empty.',
    'short_content' => 'Post must be bigger than 10 character.'
  );
  //check if empty
  static function valEmpty($val)
  {
    if (trim($val) === '') {
      return true;
    } else {
      return false;
    }
  }
  //check text length
  static function shortText($val)
  {
    $val = (string)$val;
    if (strlen($val) <= 10) {
      return true;
    }
    return false;
  }

  static function checkFile($val){
    if (self::valEmpty($val["tmp_name"])) {
      return "Picture cannot be empty.";
    }else{
      $check = getimagesize($val["tmp_name"]);
      if($check == false) {
        return "File is not an image.";
      }
      if ($val["size"] > 1500000) {
        return "Sorry, your file is too large.";
      }
    }
    return false;
  }
}