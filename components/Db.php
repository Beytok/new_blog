<?php

  class Db
  {
    private static $instance;
    public $pdo;
    private function __construct()
    {
      $db = include_once ROOT . '/config/db_params.php';
      $dsn = 'mysql:dbname='.$db['dbname'].';host='.$db['host'];
      try {
        $conn = new PDO(
          $dsn,
          $db['user'],
          $db['password'],
          [PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
        );
        $this->pdo = $conn;
      } catch (PDOException $e) {
        echo 'Подключение не удалось: ' . $e->getMessage();
      }

    }
    private function __clone() {}
    private function __wakeup() {}

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
          self::$instance = new self;
        }

        return self::$instance;
    }
  }