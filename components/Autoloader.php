<?php
//auto loader of components and models
spl_autoload_register(function ($class) {
    $path_array = array(
        'models/',
        'components/'
    );

    foreach ($path_array as $path) {
        $path = $path . $class . '.php';
        if (is_file($path)) {
            include_once "$path";
        }
    }
});