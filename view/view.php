<?php include ROOT.'/view/layouts/header.php';?>
<section class="section blog">
    <div class="container">
        <div class="row">

            <div class="col-md-9 col-sm-12 p-right">
                <div class="blog-detail">
                    <h3 class="blog-detail_title"><?=$postById['title']?></h3>
                    <div class="blog-detail_img">
                        <img src="<?=$postById['path']?>" alt="<?=$postById['title']?>">
                        <div class="blog-detail_tags">
                            <span><?=$postById['time']?></span>
                        </div>
                    </div>
                    <div class="blog-detail_text">
                      <?=nl2br($postById['content'])?>
                    </div>
                    <div class="blog-detail_tags">
                        <span id="download_csv">Download in CSV</span>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-12 pull-right">
                <div class="blog-popular">
                    <h3 class="blog-popular_name">Last post</h3>
                    <ul class="blog-popular_list">
                      <?php foreach ($newPosts as $newPost){?>
                        <li class="blog-popular_item clearfix">
                            <a href="/view/<?=$newPost['id']?>/">
                                <span class="blog-popular_img">
                                    <img src="<?=$newPost['path']?>" alt="<?=$newPost['title']?>">
                                </span>
                                <span class="blog-popular_text">
                                    <span class="blog-popular_title"><?=$newPost['title']?></span>
                                    <span class="blog-popular_comment"><?=$newPost['time']?></span>
                                </span>
                            </a>
                        </li>
                      <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include ROOT.'/view/layouts/footer.php'; ?>