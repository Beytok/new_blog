<!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="utf-8">
    <!-- <base href="/"> -->

    <title><?=$page_title?></title>
    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Template Basic Images Start -->
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="icon" href="<?=TEMPLATE_PATH?>img/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=TEMPLATE_PATH?>img/favicon/apple-touch-icon-180x180.png">
    <!-- Template Basic Images End -->

    <!-- Custom Browsers Color Start -->
    <meta name="theme-color" content="#000">

    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>css/main.css">


</head>

<body>

<header>
    <div class="top_line">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="pull-right">
                        <ul class="top-nav">
                            <li><a href="/">Home page</a></li>
                            <li><a href="/add/">Add post</a></li>
                        </ul>
                        <ul class="top-nav_right">

                            <li class="mobile-menu_icon">
                                <a href="#" class="toggle-mnu"><span></span></a>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="header-slider owl-carousel top-blur">
        <div class="header-slider_item" style="background-image: url('<?=TEMPLATE_PATH?>img/blog-bg.png');">
            <div class="top-text top-text_single">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 center-block">
                            <h1><?=$page_title?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>

<div class="breadcrumbs p-b-150"></div>