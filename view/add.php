<?php include ROOT.'/view/layouts/header.php';?>
  <section class="section">
      <form class="contact-form" action="" method="post" enctype="multipart/form-data">
        <?php if ($errors != false) {?>
            <ul id="error">
                <?php foreach ($errors as $error){?>
                    <li><?=$error?></li>
                <?php }?>
            </ul>
        <?php }?>
          <h2 class="contact-h2 m-b-30"><?=$page_title?></h2>

          <label>
            <input type="text" id="form_title" name="title" class="placeholder" placeholder="Title"
                   value=""/>
          </label>
          <label>
              <textarea rows="5" name="content" placeholder="Your post" ></textarea>
          </label>
          <div class="contact-send text-right">
            <input type="file" name="pic_file">

              <button type="submit" name="submit" class="btn btn-black"><span>Post</span></button>
          </div>
      </form>
  </section>
<?php include ROOT.'/view/layouts/footer.php'; ?>