<?php include ROOT.'/view/layouts/header.php';?>
<div class="ajax_holder">
  <?php
  if ($_REQUEST['ajax_mode'] == 'Y') {
    ob_end_clean();
  }?>
    <section class="section blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="blog-list">
                      <?php foreach($allPosts as $post){?>
                      <div class="blog-item">
                          <div class="row">
                              <div class="col-sm-5">
                                  <div class="blog-item_img">
                                      <a href="/view/<?=$post['id']?>/"><img src="<?=$post['path']?>" alt="<?=$post['title']?>"></a>
                                  </div>
                              </div>
                              <div class="col-sm-7">
                                  <div class="blog-item_body">
                                      <a href="/view/<?=$post['id']?>/" class="blog-item_link">
                                          <h3><?=$post['title']?></h3>
                                      </a>
                                      <div class="blog-item_tags">
                                          <span class="blog-item_date"><?=$post['time']?></span>
                                      </div>
                                      <p class="blog-item_text"><?=$post['content']?></p>
                                      <a href="/view/<?=$post['id']?>/" class="btn btn-black"><span>Review</span></a>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($page == 1 && !$hideButton) {?>
          <div class="blog-load_more">
              <div class="text-center">
                  <a class="show_more" data-count="<?=$more_counter?>">Show more <img src="<?=TEMPLATE_PATH?>img/load-more.png"></a>
              </div>
          </div>
        <?php }?>

    </section>
  <?php
  if ($_REQUEST['ajax_mode'] == 'Y') {
    die();
  }?>
  <?=$pagination?>
</div>
<?php include ROOT.'/view/layouts/footer.php'; ?>