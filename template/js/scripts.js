$(function() {

  var prevScrollpos = $('.header-slider').height();
  window.onscroll = function() {
    var currentScrollPos = window.pageYOffset;
    if (prevScrollpos >= currentScrollPos) {
      $(".top_line").css('top', "0px");
    } else {
      $(".top_line").css('top', "-50px");
    }
  }
  Onload();

});
// End
// Onload
function Onload() {
  $('#download_csv').unbind('click').click(function () {
    $.ajax({
      method: "POST",
      url: location.href,
      data: {ajax_mode: "Y", download: "Y"},
      success: function(response, status, xhr) {
          // check for a filename
          var filename = $('.blog-detail_title').html()+'.csv';
          var disposition = xhr.getResponseHeader('Content-Disposition');

          var type = xhr.getResponseHeader('Content-Type');
          var blob = new Blob([response], { type: type });


          var URL = window.URL || window.webkitURL;
          var downloadUrl = URL.createObjectURL(blob);

          if (filename) {
              var a = document.createElement("a");
              // safari doesn't support this yet
              if (typeof a.download === 'undefined') {
                  window.location = downloadUrl;
              } else {
                  a.href = downloadUrl;
                  a.download = filename;
                  document.body.appendChild(a);
                  a.click();
              }
          } else {
              window.location = downloadUrl;
          }

          setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
      }
    });
  });

  $('.show_more').unbind('click').click(function () {
    $.ajax({
      method: "POST",
      url: location.href,
      data: { ajax_mode: "Y", show_more: "Y", more_counter: $(this).attr('data-count')}
    })
    .done(function( msg ) {
      $('.ajax_holder').html(msg);
      Onload();
    });
  });

  initMobileMenu();
  clickOnBody();
}

function initMobileMenu() {

  $('.toggle-mnu').unbind('click').click(function(){
    $(this).toggleClass("on");
    $('.top_line').toggleClass('black');
    $(".top-nav")
      .toggleClass('active')
      .removeClass('left');
    return false;
  });

  $('.top-nav_to-second').unbind('click').click(function(){
    $(this)
      .closest('.has-child').toggleClass('open').find('.second-menu').first().toggleClass('active')
      .closest('.top-nav').toggleClass('left');
      return false;
  });

  $('.top-nav_back').unbind('click').click(function(){
    // $('.second-menu').hide();
    $(this)
      // .closest('.second-menu').hide(0)
      .closest('.top-nav').removeClass('left');
      return false;
  });
}

function clickOnBody() {
  $("body").unbind("click").click(function(event){
    // console.log($(event.target));

    if ( !$(event.target).is('nav a')) {
      $('.toggle-mnu').removeClass('on');
      $('.top-nav').removeClass('active');
      $('.top_line').removeClass('black');
      $('.second-menu').removeClass('active');
      $('.active.has-child').removeClass('open');
    }

  });
}