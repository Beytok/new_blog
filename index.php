<?php

ini_set('display_errors',1);
error_reporting(E_ALL & ~E_NOTICE);

define('ROOT', __DIR__);
define("TEMPLATE_PATH", '/template/');

//include components
include_once 'components/Autoloader.php';

$router = new Router();
$router->run();