<?php

/**
 * Description of BaseModel
 *
 * set connect to db
 */
abstract class BaseModel
{
    protected $db;

    public function __construct()
    {
        $db = Db::getInstance();
        $this->db = $db->pdo;
    }
}
