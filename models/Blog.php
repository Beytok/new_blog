<?php

class Blog extends BaseModel
{
  //option for pagination
  public $visiblePage = 3;
  public $showOnPage = 3;
  public $newElements = 5;

  public $page;
  public $controlRows;
  public $endElems;


  /**
   * string of pagination
   * @return string
   */
  public function getPagination()
  {
    $pagination = new Pagination($this);
    if ($this->showOnPage >= $this->controlRows && $this->page == 1) {
      return '';
    }
    return $pagination->setPagination();
  }

  /**
   * get all posts for home page
   * @param $page, $showMore, $newElements
   * @return array
   */
  public function getPosts($page,$showMore = false,$newElements = false)
  {
    $this->page = $page;
    //find first and last page in different mode
    $firstPage = abs(($page - 1) * $this->showOnPage);
    $endElems = $this->showOnPage * 2 + 1;

    if ($newElements) {
      $firstPage = 0;
      $endElems = $this->newElements;
    }elseif ($showMore > 0) {
      $firstPage = 0;
      $endElems = $this->showOnPage * ($showMore + 1);
      $this->showOnPage = $this->showOnPage * $showMore;
    }

    $stmt = $this->db->prepare('SELECT blog.id, blog.title, blog.content, blog.time, pic_file.path FROM blog LEFT JOIN pic_file ON blog.file = pic_file.id ORDER BY id DESC LIMIT ?, ?');
    $stmt->execute(array($firstPage,$endElems));

    $posts = $stmt->fetchAll($this->db::FETCH_ASSOC);

    $this->controlRows = count($posts);
    $this->endElems = $endElems;

    //if array of posts bigger than we need cut him
    if ($this->controlRows > $this->showOnPage && !$newElements) {
        $posts = array_slice($posts, 0, $this->showOnPage);
    }
    return $posts;
  }

  /**
   * get one post for view
   * @param $id
   * @return array
   */
  public function getPostById($id)
  {
    $stmt = $this->db->prepare('SELECT blog.id, blog.title, blog.content, blog.time, pic_file.path FROM blog LEFT JOIN pic_file ON blog.file = pic_file.id WHERE blog.id = ?');

    $stmt->execute(array($id));
    $postById = $stmt->fetch($this->db::FETCH_ASSOC);

    return $postById;
  }

  /**
   * add post to database,
   * create short content,
   * @param array $options
   * @return int
   */
  public function addPost(array $options)
  {
    $stmt = $this->db->prepare('INSERT INTO pic_file (path) VALUES (?)');
    $stmt->execute(array($options['pic_file']));

    $id = $this->db->lastInsertId();

    $stmt = $this->db->prepare('INSERT INTO blog (title, content, file) VALUES (:title, :content, :file)');
    $stmt->bindParam(':title',$options['title'],$this->db::PARAM_STR);
    $stmt->bindParam(':content',$options['content'],$this->db::PARAM_STR);
    $stmt->bindParam(':file',$id,$this->db::PARAM_INT);
    $stmt->execute();

    $postId = $this->db->lastInsertId();

    return $postId;
  }
}