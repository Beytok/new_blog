<?php


class BlogController
{
  private $blog;
  // set model
  public function __construct()
  {
    $blog = new Blog();
    $this->blog = $blog;
  }

  /**
   * displays home page
   * @param int $page
   */
  public function indexAction($page = 1)
  {
    $more_counter = 1;
    //ajax mode (show more)
    if ($_REQUEST['show_more'] == 'Y' && $_REQUEST['more_counter']) {
      $more_counter += $_REQUEST['more_counter'];
      $allPosts = $this->blog->getPosts($page,$more_counter);
      $hideButton = (
        $this->blog->controlRows < $this->blog->endElems &&
        $this->blog->controlRows == count($allPosts)
      ) ? true :false;
    }else{
    // default mode
      $allPosts = $this->blog->getPosts($page);
      $pagination = $this->blog->getPagination();
    }

    foreach ($allPosts as &$valuePost) {
      $valuePost['content'] = self::setShortContent($valuePost['content'], 350);
    }

    $page_title = 'Blog';
    include_once(ROOT . '/view/index.php');
  }

  /**
   * displays view post
   * @param $id
   */
  public function viewAction($id)
  {
    $id = (int)$id;
    $postById = $this->blog->getPostById($id);
    $page = 1;
    $newPosts = $this->blog->getPosts($page,false,true);
    foreach ($newPosts as &$newValue) {
      $newValue['content'] = self::setShortContent($newValue['content'], 150);
    }

    // ajax mode (download_csv)
    if ($_REQUEST['ajax_mode'] == 'Y' && $_REQUEST['download'] == 'Y') {
      $new_way=$_SERVER["DOCUMENT_ROOT"]."/upload/temp.csv";
      $fp = fopen($new_way, "a+");
      $arCsv = array(array_keys($postById), $postById);
      foreach ($arCsv as $fields) {
        $fields['content'] = html_entity_decode($fields['content']);
        fputcsv($fp, $fields,';');
      }
      fclose($fp);
      $downloadFile = file_get_contents($new_way);
      unlink($new_way);

      echo $downloadFile;
      die();
    }

    $page_title = 'View post';
    include_once(ROOT . '/view/view.php');
  }

  /**
   * display add form
   * check valid post
   * add to database
   */
  public function addAction()
  {
    $errors = false;
    $arRequireFilds = array(
      'title' => '',
      'content' => '',
    );

    if (isset($_POST['submit'])) {
      // validation
      $arRequireFilds = array(
        'title' => htmlentities($_POST['title']),
        'content' => htmlentities($_POST['content']),
      );

      foreach ($arRequireFilds as $fieldKey => $fieldValue) {
        if (Validator::valEmpty($fieldValue)) {
          $errors[] = Validator::$errors[$fieldKey];
        }
      }

      if (Validator::shortText($arRequireFilds['content'])) {
        $errors[] = Validator::$errors['short_content'];
      }
      if (Validator::checkFile($_FILES['pic_file']) !== false) {
        $errors[] = Validator::checkFile($_FILES['pic_file']);
      }else{
        //save file
        $target_file = '/upload/'.time().'_'.basename($_FILES["pic_file"]["name"]);

        move_uploaded_file($_FILES["pic_file"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$target_file);
        $arRequireFilds['pic_file'] = $target_file;
      }

      if ($errors === false) {
        $id = $this->blog->addPost($arRequireFilds);

        header('Location: /view/'.$id.'/');
      }
    }

    $page_title = 'Add post';
    include_once(ROOT . '/view/add.php');
  }

  public function setShortContent($strText, $intLen)
  {
    $strText = html_entity_decode($strText);
    if(strlen($strText) > $intLen){
      return rtrim(substr($strText, 0, $intLen), ".")."...";
    }else{
      return $strText;
    }
  }
}